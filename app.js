var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser')
const fs = require('fs');
var app = express();
var compression = require('compression');
var helmet = require('helmet');

//Router
var ticketRouter = require('./routes/ticket')
var indexRouter = require('./routes/index')
var downloadRouter = require('./routes/download')

// var mongoose = require('mongoose');
// var mongoDB = "mongodb+srv://lehai:Iambawmim2609@sandbox.cohcx.mongodb.net/web?retryWrites=true&w=majority";
// mongoose.connect(mongoDB, { useUnifiedTopology: true});
// var db = mongoose.connection;
// db.on('error', console.error.bind(console, 'MongoDb connection error:'));

//Version
app.locals.version = "v2.7";

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
//app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(compression());
app.use(helmet());
app.use(bodyParser.urlencoded({extended: false}))


app.use('/ticket', ticketRouter);


app.use('/',indexRouter);

//View Document content
app.use('/download', downloadRouter);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
