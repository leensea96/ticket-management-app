var express = require('express');
var router = express.Router();
os = require('os')

router.get('/healthcheck', function (req, res, next) {
    res.render('index', {version: req.app.locals.version, hostname: os.hostname()})
})

module.exports = router;