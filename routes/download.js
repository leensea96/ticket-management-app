var express = require('express');
var router = express.Router();

/* Download page. */
router.get('/', function (req, res, next) {
    res.render('download', {});
    //Use html file for form
    //res.sendFile("/Users/haile/Documents/TechStack/Code/Nodejs/Web-Development/app-test/public/html/download.html");
});

router.post('/', function (req, res, next) {
    const { GetObjectCommand } = require("@aws-sdk/client-s3");
    const { s3Client } = require("./s3Client.js");
    const bucketParams = {
        Bucket: "haile-bucket",
        Key: req.body.fileName,
    };
    
    const showFileContent = async () => {
        try {
            // Get the object from the Amazon S3 bucket. It is returned as a ReadableStream.
            const data = await s3Client.send(new GetObjectCommand(bucketParams));
            // Convert the ReadableStream to a string.
            //await data.Body.transformToString();
            let content = await data.Body.transformToString();
            //res.render('download', { fileName: req.body.fileName, content: content })
            res.set('Content-Type', 'application/json')
            res.send(content)
        } catch (err) {
            console.log("Error", err);
            res.send("Error: " + err)
        }
    };
    showFileContent();
});

module.exports = router;
