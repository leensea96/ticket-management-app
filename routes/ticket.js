var express = require('express');
var router = express.Router();
const fs = require('fs')


router.get('/list/user/:user', function (req, res, next) {
    //console.log(req.params)
    let { user } = req.params
    if (user === 'hailn2') {
        fs.readFile('./data/tickets.json', 'utf8', (err, file) => {
            if (err) {
                console.error(err);
                return;
            }
            try {
                const data = JSON.parse(file)
                var filtered = {}
                for (let ticket of Object.values(data)) {
                    if (ticket.user == 'hailn2') {
                        filtered[ticket.id] = ticket
                    }
                }
                res.render('list', { user: user, data: filtered })
            } catch (err) {
                console.log(err)
            }

        })
    }

    if (user === 'it') {
        fs.readFile('./data/tickets.json', 'utf8', (err, file) => {
            if (err) {
                console.error(err);
                return;
            }
            try {
                const data = JSON.parse(file)
                res.render('list', { user: user, data: data })
            } catch (err) {
                console.log(err)
            }

        })
    }

    if (user === 'manager1') {
        fs.readFile('./data/tickets.json', 'utf8', (err, file) => {
            if (err) {
                console.error(err);
                return;
            }
            try {
                const data = JSON.parse(file)
                var filtered = {}
                for (let ticket of Object.values(data)) {
                    if (ticket.manager == 'manager1') {
                        filtered[ticket.id] = ticket

                    }
                }
                res.render('list', { user: user, data: filtered })
            } catch (err) {
                console.log(err)
            }

        })
    }
})

router.get('/create/user/:user', function (req, res, next) {
    const { user } = req.params
    fs.readFile('./data/users.json', 'utf8', (err, file) => {
        if (err) {
            console.error(err);
            return;
        }
        try {
            const users = JSON.parse(file)

            for (let each_user of Object.values(users)) {
                if (each_user.name == user) {
                    let manager = each_user.manager
                    res.render('create', { user: user, manager: manager })
                }
            }

        } catch (err) {
            console.log(err)
        }

    })

})

router.post('/create/user/:user', function (req, res, next) {
    const filePath = 'data/tickets.json'
    fs.readFile(filePath, 'utf8', (err, file) => {
        if (err) {
            console.error(err);
            return;
        }
        try {
            const data = JSON.parse(file)
            let r = (Math.random() + 1).toString(36).substring(7);
            let ts = Date.now();
            let date_ob = new Date(ts);
            let date = date_ob.getDate();
            let month = date_ob.getMonth() + 1;
            let year = date_ob.getFullYear();

            let newTicket = {
                "id": r,
                "user": req.params.user,
                "manager": req.body.manager,
                "status": "Waiting",
                "time": date + "-" + month + "-" + year,
                "summary": req.body.summary
            }

            data[r] = newTicket
            fs.writeFileSync(filePath, JSON.stringify(data))

        } catch (err) {
            console.log(err)
        }

    })
    res.send("<p>Your request is submitted. </p><p><a href=\"/ticket/list/user/hailn2/\">Click here</a> to return Home</p>")
})

router.get("/approve/user/:manager/id/:id", (req, res, next) => {
    res.render('approve', { id: req.params.id })
})

router.post("/approve/user/:manager/id/:id", (req, res, next) => {
    if (req.body.accept == 'true') {
        const filePath = 'data/tickets.json'
        fs.readFile(filePath, 'utf8', (err, file) => {
            if (err) {
                console.error(err);
                return;
            }
            try {
                const data = JSON.parse(file)
                data[req.params.id]['status'] = 'Approved'
                fs.writeFileSync(filePath, JSON.stringify(data))
                res.send(`<p>The request is approved. </p><p><a href=\"/ticket/list/user/${req.params.manager}\">Click here</a> to return Home</p>`)
            } catch (err) {
                console.log(err)
            }

        })
    } else{
        res.redirect("/ticket/list/user/" + req.params.manager)
    }

})


module.exports = router;