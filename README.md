# Nodejs-web-app

Simple app for managing requests between users, ITs and managers. The app is then deployed on K8S environments (GKE, EKS) using Gitlab CI

## About the application

I created this app to facilitate the management of tickets in my old company. Ticket is created when a user has problems or requests with IT infrastructure (LAN connection, open port to connect to Internet,...). The old management process includes creating ticket on portal, waiting for his/her Manager to approve and waiting for confirmation from IT, most of step involve discussing via email. I writed this app to create an unified interfaces for all users to manage the lifecycle of tickets, hopefully to remove unnecessary manual steps.


## About the K8S resource



## About the CD pipeline




